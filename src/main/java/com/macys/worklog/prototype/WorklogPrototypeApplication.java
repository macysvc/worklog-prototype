package com.macys.worklog.prototype;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class WorklogPrototypeApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(WorklogPrototypeApplication.class, args);
		
	}

	

}
