package com.macys.worklog.prototype.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.pubsub.Pubsub;
import com.google.api.services.pubsub.PubsubScopes;
import com.google.common.collect.Lists;

@Component
public class WorklogService {

	@Value("${app.projectId}")
	private String projectId;

	@Value("${app.topicId}")
	private String topicId;

	@Value("${app.jsonpath}")
	private String jsonPath;

	
	public List<String> publishMessage(String data) throws IOException {
		System.out.println("Trying to publishMessage " + data + "\n JSON PATH ==" + jsonPath);
		List<com.google.api.services.pubsub.model.PubsubMessage> messages = Lists.newArrayList();
		messages.add(new com.google.api.services.pubsub.model.PubsubMessage().encodeData(data.getBytes("UTF-8")));
		com.google.api.services.pubsub.model.PublishRequest publishRequest = new com.google.api.services.pubsub.model.PublishRequest()
				.setMessages(messages);
		com.google.api.services.pubsub.model.PublishResponse publishResponse = getAuthorizedClientPubSub("").projects()
				.topics().publish(getTopic(), publishRequest).execute();

		int size = publishResponse.getMessageIds().size();
		System.out.println("Published Messages size is: " + size + "\n Response " + publishResponse.toString());
		return publishResponse.getMessageIds();
	}

	private Pubsub getAuthorizedClientPubSub(String private_key_file) throws IOException {
		Pubsub pubsub;
		HttpTransport transport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();

		GoogleCredential credential = GoogleCredential.fromStream(new FileInputStream(jsonPath));

		if (credential.createScopedRequired()) {
			credential = credential.createScoped(PubsubScopes.all());
		}
		return pubsub = new Pubsub.Builder(transport, jsonFactory, credential)
				.setApplicationName("MacysWorkLogProtoType").build();
	}

	private String getTopic() {
		return String.format("projects/%s/topics/%s", projectId, topicId);
	}

}


/*
 * public String pushMessage(String json) throws Exception {
 * 
 * // Your topic ID, eg. "my-topic"
 * 
 * String topicId = "macys-test-topic";
 * 
 * // Create a new topic ProjectTopicName topic = ProjectTopicName.of(projectId,
 * topicId); try (TopicAdminClient topicAdminClient = TopicAdminClient.create())
 * { topicAdminClient.createTopic(topic); } catch (ApiException e) { // example
 * : code = ALREADY_EXISTS(409) implies topic already exists
 * System.out.print(e.getStatusCode().getCode());
 * System.out.print(e.isRetryable()); } catch (IOException e1) { // TODO
 * Auto-generated catch block e1.printStackTrace(); }
 * 
 * System.out.printf("Topic %s:%s created.\n", topic.getProject(),
 * topic.getTopic());
 * 
 * WorklogService sendMessage = new WorklogService(); sendMessage.pushMessage();
 * 
 * 
 * System.out.println("Trying to pulish message=" + projectId + "," + topicId);
 * ProjectTopicName topicName = ProjectTopicName.of(projectId, topicId);
 * Publisher publisher = null; List<ApiFuture<String>> futures = new
 * ArrayList<>();
 * 
 * try { // Create a publisher instance with default settings bound to the topic
 * publisher = Publisher.newBuilder(topicName).build();
 * 
 * 
 * String message = json; System.out.println("Pushing message" + message); //
 * convert message to bytes ByteString data = ByteString.copyFromUtf8(message);
 * com.google.pubsub.v1.PubsubMessage pubsubMessage =
 * com.google.pubsub.v1.PubsubMessage.newBuilder().setData(data).build();
 * 
 * // Schedule a message to be published. Messages are automatically batched.
 * ApiFuture<String> future = publisher.publish(pubsubMessage);
 * futures.add(future);
 * 
 * } finally { // Wait on any pending requests List<String> messageIds =
 * ApiFutures.allAsList(futures).get();
 * System.out.println("You are in finally!!"); for (String messageId :
 * messageIds) { System.out.println(messageId); }
 * 
 * if (publisher != null) { // When finished with the publisher, shutdown to
 * free up resources. publisher.shutdown(); } } return null; }
 */
