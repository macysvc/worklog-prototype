package com.macys.worklog.prototype.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.macys.worklog.prototype.service.WorklogService;


@RestController

@RequestMapping("/worklog-service/pubsub")
public class WorklogController {

	@Autowired
	private WorklogService worklogService;
	
	@RequestMapping(value = "/publishMessage", method = RequestMethod.POST)
	public String publishMessage(@RequestBody String json) {
		try {
			return worklogService.publishMessage(json).toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Failed to publish message.";				
	}
}
